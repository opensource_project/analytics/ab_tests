import scipy.stats as stats
import numpy as np 
import pandas as pd

from matplotlib import pyplot as plt
import seaborn as sns


def compute_sample_size_atb(epsilon, std, alpha=0.05, beta=0.2):
    t_alpha = stats.norm.ppf(1 - alpha / 2, loc=0, scale=1)
    t_beta = stats.norm.ppf(1 - beta, loc=0, scale=1)
    z_scores_sum_squared = (t_alpha + t_beta) ** 2
    sample_size = int(
        np.ceil(
            z_scores_sum_squared * (2 * std ** 2) / (epsilon ** 2)
        )
    )
    return sample_size

def compute_sample_size_arb(mu, std, eff=1.01, alpha=0.05, beta=0.2):
    epsilon = (eff - 1) * mu
    return compute_sample_size_atb(epsilon, std=std, alpha=alpha, beta=beta)

def compute_ttest(a, b, alpha=0.05):
    """Тест Стьюдента. Возвращает 1, если отличия значимы."""
    _, pvalue = stats.ttest_ind(a, b)
    return int(pvalue < alpha)


def compute_table_sample_size(mu, std, effects, errors):
    '''
    MDE для теста выборочных средних
    mu - выборочное среднее
    std - выборочное стандартное отклонение
    effects:list - список эффектов в процентах
    errors: спсок значимости
    '''
    results = []
    for eff in effects:
        results_eff = []
        for err in errors:
            results_eff.append(
                compute_sample_size_arb(
                    mu,
                    std,
                    eff=eff,
                    alpha=err,
                    beta=err
                )
            )
        results.append(results_eff)
        
    df_results = pd.DataFrame(results)
    df_results.index = pd.MultiIndex(
        levels=[[f'{np.round((x - 1)*100, 1)}%' for x in effects]],
        codes=[np.arange(len(effects))],
        names=['effects']
    )
    df_results.columns = pd.MultiIndex.from_tuples(
        [(err, ) for err in errors],
        names=['errors']
    )
    return df_results

# В случае если размер выборки ограничен, мы можем оценить 
# Какой минимальный эффект нужно отслеживать, чтобы
# получить значимые результаты теста

def get_minimal_determinable_effect(std, sample_size, alpha=0.05, beta=0.2):
    t_alpha = stats.norm.ppf(1 - alpha / 2, loc=0, scale=1)
    t_beta = stats.norm.ppf(1 - beta, loc=0, scale=1)
    disp_sum_sqrt = (2 * (std ** 2)) ** 0.5
    mde = (t_alpha + t_beta) * disp_sum_sqrt / np.sqrt(sample_size)
    return mde

def get_minimal_determinable_effect_binom(k, p, sample_size, alpha=0.05, beta=0.2):
    t_alpha = stats.norm.ppf(1 - alpha / 2, loc=0, scale=1)
    t_beta = stats.norm.ppf(1 - beta, loc=0, scale=1)
    MDE = t_alpha * (((p * (1-p))/ (sample_size) / k) ** 0.5) + t_beta * (((p * (1-p))/ (sample_size) / (k + 1)) ** 0.5)
    return MDE

def plot_pvalue_ecdf(pvalues, title=None):

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 4))
    if title:
        plt.suptitle(title)

    sns.histplot(pvalues, ax=ax1, bins=20, stat='density')
    ax1.plot([0,1],[1,1], 'k--')
    ax1.set(xlabel='p-value', ylabel='Density')

    sns.ecdfplot(pvalues, ax=ax2)
    ax2.plot([0,1],[0,1], 'k--')
    ax2.set(xlabel='p-value', ylabel='Probability')
    ax2.grid()

    
