
import scipy.stats as stats
from statsmodels.stats.proportion import proportions_ztest
import numpy as np
from scipy.stats import kendalltau
from .utils.stat_tests import get_minimal_determinable_effect, compute_sample_size_atb, compute_table_sample_size
from .utils.stat_tests import plot_pvalue_ecdf
import plotly.graph_objects as go

class Design():

    def __init__(self):
        return
    
    def check_effect(self, df_pilot, df_control, metric_name, type='numeric', test='mean'):
        if type == 'numeric':
            if test == 'mean':
                a_value = df_pilot[metric_name].mean()
                b_value = df_control[metric_name].mean()
                effect = a_value - b_value
                effect_percent = effect / b_value
        elif type == 'binary':
            a_value = df_pilot[metric_name].sum() / df_pilot[metric_name].sum()
            b_value = df_control[metric_name].sum() / df_control[metric_name].sum()
            effect = a_value - b_value
            effect_percent = effect * 100 / b_value
        return a_value, b_value, effect, effect_percent

    def check_mde(self, df, metric_name, alpha=0.05, beta=0.2):
        std = df[metric_name].std()
        sample_size = len(df)
        
        mde = get_minimal_determinable_effect(
            std=std,
            sample_size=sample_size,
            alpha=alpha,
            beta=beta
        )
        return mde
    
    def check_sample_size_atb(self, df, metric_name, effect, alpha=0.05, beta=0.2):
        std = df[metric_name].std()
        size = compute_sample_size_atb(
            epsilon=effect,
            std=std,
            alpha=alpha,
            beta=beta
        )
        return size
    
    def check_effects_and_sizes(self, df, metric_name, effects, errors):
        mu = df[metric_name].mean()
        std = df[metric_name].std()
        table = compute_table_sample_size(
            mu=mu, std=std,
            effects=effects,
            errors=errors
        )
        return table
    
    def check_trends(self, df, metric_name, date_name):
        table = df.groupby(date_name)[metric_name].mean().reset_index()
        graph = go.Figure()
        graph.add_trace(
            go.Bar(
                x=table[date_name],
                y=table[metric_name]
            )
        )
        tau, p_value = test_trend = kendalltau(
            np.arange(len(df)),
            df.sort_values(date_name)[metric_name]
        )
        report = f'Kendall tau correlation: {tau:.3f}'
        report += f'\nP-value: {p_value:.3f}'

        # Interpretation
        alpha = 0.05  # or 0.01 for a 1% significance level
        if p_value < alpha:
            report += '\nThe trend is significant.'
        else:
            report += '\nThe trend is not significant.'
        return graph, report
    
    def compute_p_value_stab(self, df, metric_name, id_name, alpha=0.05, number_tests=300, effect=1, metric_type='numeric'):
        p_values = []
        keys_unique = df[id_name].unique()
        def check_true(p_value):
            if p_value < alpha:
                return 1
            else:
                return 0

        for _ in range(number_tests):
            np.random.shuffle(keys_unique)
            group_a, group_b = keys_unique[:len(keys_unique) // 2], keys_unique[len(keys_unique) // 2:]
            
            metric_a = df[df[id_name].isin(group_a)][metric_name]
            metric_b = df[df[id_name].isin(group_b)][metric_name]
            if metric_type == 'numeric':
                if effect != None:
                    metric_a = metric_a * effect
                _, p_val = stats.ttest_ind(metric_a, metric_b)
            elif metric_type == 'binom':
                successes_control = metric_a.sum() * effect
                successes_test = metric_b.sum()
                nobs_control = len(metric_a)
                nobs_test = len(metric_b)

                # Количество успехов и общее количество наблюдений
                count = [successes_control, successes_test]
                nobs = [nobs_control, nobs_test]

                # Проведение Z-теста
                stat, p_val = proportions_ztest(count, nobs)
            p_values.append(p_val)
        var = list(map(check_true, p_values))
        plot_pvalue_ecdf(p_values, 'test')
