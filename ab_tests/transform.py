import pandas as pd
import numpy as np
import scipy.stats as stats

class Transform:

    def __init__(self):
        return 
    
    def calculate_theta(self, df, metric_name, covariate_name) -> float:
        """Вычисляем Theta."""
        covariance = np.cov(df[metric_name], df[covariate_name])[0, 1]
        variance = df[covariate_name].var()
        theta = covariance / variance
        return theta

    def cuped(self, df, metric_pilot, metric_control, covariate_name):
        '''
        
            Применяет CUPED трансформатцию 
            к текущей количественной метрике. 
            Выборки должны быть смерджены перед расчетом
            по соотвествующим су
        '''
        df_all = df.fillna(0)
        # Ковариация деленная на дисперсию
        theta = self.calculate_theta(df_all, metric_pilot, covariate_name)
        df_all['cuped_pilot'] = df_all[metric_pilot] - theta * df_all[covariate_name] # - mu_last
        df_all['cuped_control'] = df_all[metric_control] - theta * df_all[covariate_name]
        return df_all
    
    def linearization(self, df_control, df_pilot, metric_up, metric_bottom):
        '''
        Линеаризует значения 2-х пользовательских метрик на основе значений контрольной группы.

        :param df_control: pd.DataFrame - Контрольная группа
        :param df_pilot: pd.DataFrame - Пилотная группа
        :param metric_up: str - Знаменатель, название ключа в переданных группах
        :param metric_bottom: str -  Числитель, название ключа в переданных группах

        :return: Возвращает переданные группы с дополнительным столбцом ['linear_metric']

        '''
        all_ration = df_control[metric_up].sum() / df_control[metric_bottom].sum()
        df_control['linear_metric'] = df_control[metric_up] - all_ration * df_control[metric_bottom]
        df_pilot['linear_metric'] = df_pilot[metric_up] - all_ration * df_pilot[metric_bottom]
        return df_control, df_pilot
        