
import plotly.express as px
import pandas as pd
import numpy as np 
import scipy.stats as stats

class Testing:

    def __init__(self):
        return 
    

    def bootstrap(self, A:pd.DataFrame, B:pd.DataFrame, func, metric_name, number_strap=300, ci="norm"):
        ''' 
        Если 0 попал в доверительный интервал империчского распределения метрик, то
        отличия не статистически значимы. 

        :param func: Функция на основе 2-х столбцов, которая возвращает агрегацию
        :param ci: str: in ["norm", "percentile", "pivotal"] - подбирается в зависимости 
        от распределения. default - "norm"
        '''
        result = []
        re_point = func(A[metric_name], B[metric_name])
        for i in range(number_strap):
            a = np.random.choice(A[metric_name], size=len(A[metric_name]), replace=True)
            b = np.random.choice(B[metric_name], size=len(B[metric_name]), replace=True)
            point = func(a, b)
            result.append(point)
        ci_interval = None
        if ci == 'norm':
            interval_func = self.get_ci_bootstrap_normal 
            left, right = interval_func(boot_metrics=result, pe_metric=re_point)
        elif ci == 'percentile':
            interval_func = self.get_ci_bootstrap_percentile
            left, right = interval_func(boot_metrics=result, pe_metric=re_point)
        elif ci == "pivot":
            interval_func = self.get_ci_bootstrap_pivotal
            left, right = interval_func(boot_metrics=result, pe_metric=re_point)
        ci_interval = [left, right]
        df_result = pd.DataFrame(result, columns=['bootstrap'])
        px.histogram(df_result, x="bootstrap").show()
        report = ""
        if left < 0 and right > 0:
            report += "Отличия статистически НЕ значимы."
        else:
            report += "Отличия статистически значимы."
        return result, ci_interval, report


    def get_ci_bootstrap_normal(self, boot_metrics: np.array, pe_metric: float, alpha: float=0.05):
        """Строит нормальный доверительный интервал.

        boot_metrics - значения метрики, полученные с помощью бутстрепа
        pe_metric - точечная оценка метрики
        alpha - уровень значимости
        
        return: (left, right) - границы доверительного интервала.
        """
        c = stats.norm.ppf(1 - alpha / 2)
        se = np.std(boot_metrics)
        left, right = pe_metric - c * se, pe_metric + c * se
        return left, right


    def get_ci_bootstrap_percentile(self, boot_metrics: np.array, pe_metric: float, alpha: float=0.05):
        """Строит доверительный интервал на процентилях.

        boot_metrics - значения метрики, полученные с помощью бутстрепа
        pe_metric - точечная оценка метрики
        alpha - уровень значимости
        
        return: (left, right) - границы доверительного интервала.
        """
        left, right = np.quantile(boot_metrics, [alpha / 2, 1 - alpha / 2])
        return left, right


    def get_ci_bootstrap_pivotal(self, boot_metrics: np.array, pe_metric: float, alpha: float=0.05):
        """Строит центральный доверительный интервал.

        boot_metrics - значения метрики, полученные с помощью бутстрепа
        pe_metric - точечная оценка метрики
        alpha - уровень значимости
        
        return: (left, right) - границы доверительного интервала.
        """
        right, left = 2 * pe_metric - np.quantile(boot_metrics, [alpha / 2, 1 - alpha / 2])
        return left, right