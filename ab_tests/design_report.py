import numpy as np
from .ab_design import Design


class ReportABDesign(Design):

    def __init__(self):
        return

    def check_testing_design(self, df, metric_name, need_absolute_effect=None, date_name=None):
        '''
            Передаются данные до проведения эксперимента.

            Функция проверяет корректность дизайна эксперимента на
            исторических данных. Подразумевается что группа 
            примерно похожа на будущую группу для эксперимента.

            На основе этой группе алгоритм вернет

            1) Минимальный детектируемый эффект (MDE)
            2) Какое кол-во наблюдений нужно собрать
                чтобы получить желаемый эффект
            3) Таблицу возможного определенного эффекта
                по размеру выборки

            4) График периодичности и статистика периодичности данных
                если передан столбец с датой

        '''
        mde = self.check_mde(
            df=df,
            metric_name=metric_name
        )
        len_sample = len(df)
        std = df[metric_name].std()

        #
        effects = np.linspace(1.05, 1.5, 10)
        errors = [0.05, 0.1, 0.15, 0.2]
        table = self.check_effects_and_sizes(
            df=df,
            metric_name=metric_name,
            effects=effects,
            errors=errors
        )
        report = f"""
        std: {std};
        Кол-во наблюдений: {len_sample};
        MDE: {mde};
        """
        if date_name != None:
            trend_graph, trend_report = self.check_trends(
                df=df,
                metric_name=metric_name,
                date_name=date_name
            )
        else:
            trend_graph = None
            trend_report = None
        if need_absolute_effect != None:
            need_sample_size = self.check_sample_size_atb(
                df=df,
                metric_name=metric_name,
                effect=need_absolute_effect
            )
            ef = mde - need_absolute_effect
            otn = round(mde / need_absolute_effect, 3)
            pri_pos = round(df[metric_name].mean() + mde, 3)
            pri_neg = round(df[metric_name].mean() - mde, 3)
            if ef > 0:
                report += f"\nВы можете отследить эффект в {otn} больший, нежели хотите, измените дизайн."
                report += f"\nТо есть, только если ваша метрика выростет до >{pri_pos} или упадет до <{pri_neg}, изменения будут стат значимы."
            else:
                report += f"\nВы можете отследить эффект в {otn} меньший, нежели хотите."
            report += f"\nДля отслеживания эффекта в {need_absolute_effect} соберите {need_sample_size} наблюдений,"
            report += f"\nлибо измените дизайн тестирования в соотвествии с таблицей ошибок и эффектов."
            
        return report, table, trend_graph, trend_report
    
    
    def aa_test(self, df, metric_name, id_name, effect=None, metric_type='numeric'):
        self.compute_p_value_stab(
            df=df, metric_name=metric_name,
            id_name=id_name, effect=effect,
            metric_type=metric_type
        )
